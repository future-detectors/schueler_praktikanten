# What to do with Schülerpraktikanten?
Date: July 19 2021

## 0. Idea
Give them an introduction into the working principle of an experimental physicist, e.g.:
- What is the project? Detector physics, detector R&D, calorimetry.
- Know the theory -> build & know every part of your detector -> do your measurement -> do the analysis & get results.

## 1. Topic introduction (1h)
- Frank gives them an introduction what we do in this group.

## 2. Hardware Introduction (~1h min)
- Introduce and show a rare Claws board.
- Introduce the scinillating tiles wrapped and unwrapped (under UV light if possible).
- Have a look at the SiPM with the microscope to the single pixels.
- Why do we wrap the scintillating tile?
- Make them wrap a tile. (extra 15min if non-sticky foil is used)

## 3. First measurement - Hands-on SiPM (~1h-1.5h)
- Install a bare Claws board in the climate chamber.
- Show them the 1 & 2 p.e. signals in the persistence mode -> make the connection to the microsope picture. Let them play around with the 1 and 2 pe signals.
- The source will be used soon. Explain them briefly how a beta emitter works. **(Ask the praktikanten to leave the room for the time of the source installation -> LAB SAFETY)**
- Put first a non-packaged tile on the SiPM with the source on top. Increase the trigger (repeat-mode) step by step and remember until which amplitude signals have been observed. Do the same with a packaged tile and find out, that the packaged tile show higher amplitudes.

## 4a. Second Measurement - Muotelescope (~1-1.5h)
- Let them plan and set up a muon telescope measurement with three boards in total. Two should be close together for an energy spectrum measurement. The third one is around two time bins away (~25cm distance with 400ps sampling rate) in order to measure the speed of light/muon.
- Let them test the system & wait for one or two muon events.
- Do an overnight measurement.
- Save the events in text format and as waveforms in the picoscope software.

## 4b. Do the overnight measurement analysis (1.5h)
- Let them find the few events in which the most remote tile has an event too. Measure the speed of light by using the functions in the picoscope software.
- Use the jupyter notebook to read-in the data and plot the energy histogram. Explain before, what a histogram is and how it works.



