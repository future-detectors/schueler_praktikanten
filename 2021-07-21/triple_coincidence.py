import pandas as pd
import matplotlib.pyplot as plt

interesting_files = [35, 42, 43, 44, 45, 46, 148, 200, 258, 259, 442, 494, 518, 527, 704, 814]
threshold = -25

for file_num in interesting_files:
	df = pd.read_csv("20210721-0001/20210721-0001_{:03d}.csv".format(file_num), header=0, skiprows=[1,2])

	time_chA = 0
	time_chE = 0

	for index in range(df.shape[0]):
		voltage = df.loc[index, "Channel E"]
		if voltage < threshold:
			time_chE = df.loc[index, "Time"]
			break

	for index in range(df.shape[0]):
		voltage = df.loc[index, "Channel A"]
		if voltage < threshold:
			time_chA = df.loc[index, "Time"]
			break

	delta_t = time_chE - time_chA
	c = 2.65e8/delta_t
	print(time_chE - time_chA, c/1e8)
